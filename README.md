# Prune

## Prune Docker Images

Ansible Playbook for maintaining Container virtualization Servers (Docker) by removing unused Images.

https://docs.ansible.com/ansible/latest/modules/docker_prune_module.html

<p>
<img src="https://gitlab.com/pascal.cantaluppi/prune/-/raw/master/img/prune.png" alt="Prune" />
</p>
